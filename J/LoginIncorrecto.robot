*** Settings ***
Documentation       Este caso hace parte del ingreso incorrecto como administrador 
Resource    Resources.robot
Library             Selenium2Library 

*** Keywords ***
Presionar Boton
    Click Button      xpath=/html/body/form/input[3]

Mostrar Alerta
    Clear Element Text      xpath=/html/body/form/input[2]
    Click Button      xpath=/html/body/form/input[3]

*** Test Cases ***
G001 Ingreso Login incorrecto como administrador 
    Open Browser      ${URL}     ${Navegador}
    Maximize Browser Window
    Set Selenium Speed	${timeout}
    Input Text      xpath=/html/body/form/input[1]]     ${Usuario}
    Input Password      xpath=/html/body/form/input[2]     ${Clave}
    Click Button      xpath=/html/body/form/input[3]      
    Set Screenshot Directory	     Img
    Capture Page Screenshot	    login.png