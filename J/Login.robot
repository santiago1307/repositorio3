*** Settings ***
Documentation       Este caso hace parte del ingreso como administrador
Resource    Resources.robot
Library             Selenium2Library 

*** Keywords ***
Presionar Boton
    Click Button      xpath=/html/body/form/input[3]

Mostrar Alerta
    Clear Element Text      xpath=/html/body/form/input[2]
    Click Button      xpath=/html/body/form/input[3]

*** Test Cases ***
G001 Ingreso Login como administrador 
    Open Browser      ${URL}     ${Navegador}
    Maximize Browser Window
    Set Selenium Speed	${timeout}
    Input Text      xpath=/html/body/form/input[1]]     ${Usuario}
    Input Password      xpath=/html/body/form/input[2]     ${Clave}
    Run Keyword If      '${Usuario}'=='administrador@redstone.com' and '${Clave}'=='123'    Presionar Boton
	Sleep  5s
    Set Screenshot Directory	     Img
    Capture Page Screenshot	    login.png