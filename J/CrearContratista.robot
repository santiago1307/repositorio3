*** Settings ***
Documentation       Este caso hace parte del ingreso como administrador
Resource    Resources.robot
Library             Selenium2Library

*** Keywords ***
Completar formulario
    Input Text  xpath=//*[@id="contenido"]/div[1]/div[1]/input   1193517295
	Input Text  xpath=//*[@id="contenido"]/div[1]/div[3]/input   3126652538
	Input Text  xpath=//*[@id="contenido"]/div[1]/div[2]/input    Santiago Orozco
	Input Text  xpath=//*[@id="contenido"]/div[1]/div[4]/input    santiago_orozco23191@elpoli.edu
	Press Key       xpath=//*[@id="inlineFormCustomSelectPref"]  \\13
    Click Element   xpath=//*[@id="inlineFormCustomSelectPref"]/option[2]
	Click Element   name:estado
	Click Element   xpath=//*[@id="contenido"]/div[2]/button
	
	
*** Test Cases ***
G005 Crear contratista
	Ingreso como administrador
	Click Element       xpath=//*[@id="tres"]	
	Set Selenium Speed	${timeout}
	Title Should Be      INTEROBRAS
	Click Element       xpath=/html/body/div[2]/div[1]/button  
    Completar formulario
	Handle Alert	timeout=10 s
	Click Element       xpath=//*[@id="optionli"]/a   \\13
	Click Element       xpath=/html/body/header/a[1]   \\13
	Set Screenshot Directory	     Img
    Capture Page Screenshot	    crearContratista.png