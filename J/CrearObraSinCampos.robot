*** Settings ***
Documentation       Este caso hace parte del ingreso como administrador
Resource    Resources.robot
Library             Selenium2Library

*** Keywords ***
Completar formulario

    Input Text      xpath=//*[@id="contenido"]/div[1]/div[1]/input     ${NombreObra} 
    Input Text      xpath=//*[@id="contenido"]/div[1]/div[3]/input     ${ValorObra}
	Press Key       xpath=//*[@id="inlineFormCustomSelectPref"]  \\13
    Click Element       xpath=//*[@id="inlineFormCustomSelectPref"]/option[3]
	Click Element       name:estado
	Select From List By Label    estado  Contratado
	Press Key       xpath=//*[@id="contenido"]/div[2]/button   \\13
	
	
*** Test Cases ***
G004 Crear obra sin campos completados
	Ingreso como administrador
	Press Keys       xpath=/html/body/header/a[1]   \\13	
	Set Selenium Speed	${timeout}
	Title Should Be      INTEROBRAS
	Click Element       xpath=//*[@id="uno"]
	Click Element     xpath=/html/body/div[2]/div/button/a
	Completar formulario
	Set Screenshot Directory	     Img
    Capture Page Screenshot	    crearObraNoHappy.png