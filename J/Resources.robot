** Settings ***
Library             Selenium2Library

*** Variables ***
${Navegador}    chrome
${URL}      http://localhost:8080/prueba%20de%20sesion
${timeout}    1 seconds
${Usuario}    administrador@redstone.com
${Clave}    123
${NombreObra}    Edificio altamira
${DireccionObra}    Cra.50 # 20b - 10
${ValorObra}    674000000000
${CedulaContratista}    43650734 
${NombreContratista}    Jorge Soto Alvarez
${CelularContratista}   3013546787
${CorreoContratista}    JorgeSoto@redstone.com
${FechaInicioContrato}    25/02/2020
${FechaFinContrato}    20/05/2020

*** Keywords ***
Ingreso como administrador
    Open Browser      ${URL}     ${Navegador}
    Maximize Browser Window
    Set Selenium Speed	1 seconds
    Input Text      xpath=/html/body/form/input[1]     ${Usuario}
    Input Password      xpath=/html/body/form/input[2]     ${Clave}
    Click Button      xpath=/html/body/form/input[3]
    Set Screenshot Directory	     Img
    Capture Page Screenshot	    login.png
	
