*** Settings ***
Documentation       Este caso hace parte del ingreso como administrador
Resource    Resources.robot
Library             Selenium2Library

*** Keywords ***
Completar formulario
    Input Text      xpath=//*[@id="contenido"]/div[1]/div[1]/input     ${NombreObra} 
    Input Text      xpath=//*[@id="contenido"]/div[1]/div[2]/input     ${DireccionObra}
    Input Text      xpath=//*[@id="contenido"]/div[1]/div[3]/input     ${ValorObra}
	Press Key       xpath=//*[@id="inlineFormCustomSelectPref"]  \\13
    Click Element       xpath=//*[@id="inlineFormCustomSelectPref"]/option[3]
	Click Element       name:estado
	Select From List By Label    estado  Contratado
	Press Key       xpath=//*[@id="contenido"]/div[2]/button   \\13
	
	
*** Test Cases ***
G003 Crear obra
	Ingreso como administrador
	Press Key       xpath=/html/body/header/a[1]   \\13	
	Set Selenium Speed	${timeout}
	Title Should Be      RedStone
	Press Key       xpath=/html/body/div[2]/div/button/a   \\13  
    Completar formulario
	Handle Alert	timeout=10 s
	Set Screenshot Directory	     Img
    Capture Page Screenshot	    crearObra.png

  