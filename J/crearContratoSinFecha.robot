*** Settings ***
Documentation       Este caso hace parte del ingreso como administrador
Resource    Resources.robot
Library             Selenium2Library

*** Keywords ***
Completar formulario
    Press Key       xpath=//*[@id="contenido"]/div[1]/div[1]/select 	\\13
	Click Element   xpath=//*[@id="contenido"]/div[1]/div[1]/select/option[3]
	Click Element   name:obras
	Press Key       xpath=//*[@id="contenido"]/div[1]/div[2]/select		\\13
	Click Element   xpath=//*[@id="contenido"]/div[1]/div[2]/select/option[3] 
	Click Element   name:contratista
	Input Text      xpath=//*[@id="contenido"]/div[1]/div[4]/input  	 20/07/2020
	Click Element   xpath=//*[@id="contenido"]/div[2]/button
	
*** Test Cases ***
G009 Hacer click en guardar sin rellenar el campo fecha
	Ingreso como administrador
	Click Element       xpath=//*[@id="cuatro"]	
	Set Selenium Speed	${timeout}
	Title Should Be      INTEROBRAS
	Click Element       xpath=/html/body/div[2]/div/button/a
    Completar formulario
	Set Screenshot Directory	     Img
    Capture Page Screenshot	    crearContratoNoHappy.png